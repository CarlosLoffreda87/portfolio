export const techs = [
  {
    id: 1,
    name: "HTML",
    description:
      "Utilicé éste lenguage de marcado de hiper texto para estructurar mi portfolio y de éste modo poder añadir imágenes, párrafos, textos etc...",
  },
  {
    id: 2,
    name: "CSS",
    description:
      "Una vez realizada la estructuración del sitio web, gracias a css, logré darle estilos a mi portfolio para que tenga un mejor aspecto",
  },
  {
    id: 3,
    name: "JAVASCRIPT",
    description:
      "Gracias a Javascript, logré agregarle un pocó de lógica a mi sitio web. De ésta forma, quién lo visite podrá interactuar un poco con las opciones que el sitio ofrece haciendo más agradable la experiencia del usuario.",
  },
  {
    id: 4,
    name: "REACT.JS",
    description:
      "Gracias a ésta librería de Javascript, logré optimizar el funcionamiento del sitio web. Aprovechando la posibilidad de estructurar mi códico en distintos componentes reutilizables",
  },
];
