import React from 'react'
import styles from "./TechnologiesCard.module.css"

function TechnologiesCard({tech}) {


  const links = [
    "https://developer.mozilla.org/en-US/docs/Web/HTML",
    "https://developer.mozilla.org/en-US/docs/Web/CSS",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript",
    "https://legacy.reactjs.org/",
  ]
  
  return (
    <a href={tech.id == 1? links[0] : tech.id == 2? links[1] : tech.id == 3? links[2] : tech.id == 4? links[3] : null} className={tech.id == 1? styles.html : tech.id == 2? styles.css : tech.id == 3? styles.js : tech.id == 4? styles.react : null} target='blank'>
      <h2>{tech.name}</h2>
      <p>{tech.description}</p>
    </a>
  )
}

export default TechnologiesCard