import React from "react";
import "./Contact.css";
import gmailImage from "../images/gmail.png";
import phoneImage from "../images/phone.png";
import houseImage from "../images/house.png";
import linkedinImage from "../images/linkedin.png";

function Contact() {
  return (
    <div className="container-contact">
      <h1>Contacto</h1>
      <div className="info-container">
        <div className="info">
          <img src={phoneImage} alt="phoneImage" />
          <p>(+54) 11-5816-7284</p>
        </div>
        <div className="info">
          <img src={gmailImage} alt="gmailImage" />
          <p>carlosloffmil987@gmail.com.ar</p>
        </div>
        <div className="info">
          <img src={houseImage} alt="addressImage" />
          <p>
            Av. Justo José de Urquiza 5036, Caseros, Provincia de Buenos Aires
          </p>
        </div>
        <div className="info">
          <img src={linkedinImage} alt="linkedinImage" />
          <p>
            {" "}
            <a href="https://www.linkedin.com/in/carlos-loffreda-3568711a2/" target="blank">
              https://www.linkedin.com/in/carlos-loffreda-3568711a2/
            </a>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Contact;
