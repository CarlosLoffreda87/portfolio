import React from "react";
import TechnologiesCard from "../TechnologiesCard/TechnologiesCard";
import "./Technologies.css";

function Technologies({ techs }) {
  return (
    <>
      <h2 className="technologies">Tecnologías</h2>
      <div className="tech-container">
        {techs.map((tech) => (
          <TechnologiesCard tech={tech} key={tech.id} />
        ))}
      </div>
    </>
  );
}

export default Technologies;
