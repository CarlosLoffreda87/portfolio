import React from "react";
import { Drawer, Box, IconButton } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useState } from "react";
import styles from "./MenuButton.module.css";
/* import { animateScroll as scroll } from "react-scroll"; */

function MenuButton({
  scrollAboutMe,
  scrollContact,
  scrollTech,
  scrollProjects,
}) {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  return (
    <div>
      <div className={styles.iconBUtton}>
        <IconButton onClick={() => setIsDrawerOpen(true)}>
          <MenuIcon />
        </IconButton>
      </div>

      <Drawer
        anchor="right"
        open={isDrawerOpen}
        onClose={() => setIsDrawerOpen(false)}
        sx={{position: "absolute", zIndex: "5"}}
      >
        <Box
          p={2}
          width="250px"
          height="100%"
          textAlign="center"
          role="presentation"
          backgroundColor="black"
        >
          <nav className={styles.navButtonDrawer}>
            <button id="aboutMe" onClick={scrollAboutMe}>
              Sobre mi
            </button>
            <button id="contact" onClick={scrollContact}>
              Contacto
            </button>
            <button id="tech" onClick={scrollTech}>
              Tecnologías
            </button>
            <button id="tech" onClick={scrollProjects}>
              Proyectos
            </button>
          </nav>
        </Box>
      </Drawer>
    </div>
  );
}

export default MenuButton;
