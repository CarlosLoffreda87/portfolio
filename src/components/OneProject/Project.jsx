import React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import styles from "./Project.module.css";



function Project() {
  return (
    <div className={styles.projectContainer}>
      <h1>Proyectos</h1>
      <hr className={styles.divisor}/>
      <div className={styles.accordionContainer}>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <h3>Carlos-Tasks</h3>
          </AccordionSummary>
          <AccordionDetails>
            <p>
              Carlos-Tasks es un proyecto simple para agregar y quitar tareas,
              realicé algunas validaciones para completar los campos de forma
              correcta. Al igual que mi portfolio, está realizada con React.js.
            </p>
            <a
              href="https://carlos-tasks.netlify.app/"
              className={styles.link}
              target="blank"
            >
              Probar
            </a>
          </AccordionDetails>
        </Accordion>
      </div>
    </div>
  );
}

export default Project;
