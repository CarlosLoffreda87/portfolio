import React from "react";
import styles from "./AppBar.module.css";
import headerImage from "../images/Carlos.jpg";
import { animateScroll as scroll } from "react-scroll";
import MenuButton from "../MenuButton/MenuButton";

function AppBar() {
  window.addEventListener("scroll", function () {
    const header = this.document.querySelector("header");
    header.classList.toggle(styles.newContainer, this.window.scrollY > 0);
    header.classList.toggle(styles.newNav, this.window.scrollY > 0);
  });

  function scrollContact() {
    window.scrollTo({ top: 1520, behavior: "smooth" });
  }

  function scrollAboutMe() {
    scroll.scrollToTop({ behavior: "smooth" });
  }

  function scrollTech() {
    window.scrollTo({ top: 2140, behavior: "smooth" });
  }

  function scrollProjects() {
    scroll.scrollToBottom({ behavior: "smooth" });
  }

  return (
    <header className={styles.container}>
      <section className={styles.name}>
        <img src={headerImage} alt="headerImage" />
        <h3>Carlos Loffreda</h3>
      </section>
      <nav className={styles.nav}>
        <button id="aboutMe" onClick={scrollAboutMe}>
          Sobre mi
        </button>
        <button id="contact" onClick={scrollContact}>
          Contacto
        </button>
        <button id="tech" onClick={scrollTech}>
          Tecnologías
        </button>
        <button id="tech" onClick={scrollProjects}>
          Proyectos
        </button>
      </nav>
      <div className={styles.burgerButton}>
      <MenuButton scrollAboutMe={scrollAboutMe} scrollContact={scrollContact} scrollTech={scrollTech} scrollProjects={scrollProjects}/>
      </div>
    </header>
  );
}

export default AppBar;
