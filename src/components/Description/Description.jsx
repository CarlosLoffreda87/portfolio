import React, { forwardRef } from "react";
import "./Description.css";

function Description() {
  return (
    <div className="container-description" id="description">
      <h1>Frontend Developer</h1>
      <p>
        Mi nombre es <span>Carlos Loffreda</span> y soy frontend web developer.
        Soy un desarrollador web con muchas ganas de aprender nuevas tecnologías
        constantemente. Me gustan los desafíos, los tomo con mucha
        responsabilidad y con la certeza de que son indispensables para mi
        crecimiento profesional. Valoro el buen ambiente laboral pero también me gusta generarlo. También me gusta sentirme parte del impacto positivo que tiene la tecnología en la vida de las personas.
      </p>
      <p>
        Realicé un <span>curso de Programación web en Digital House</span> de
        112 horas de clases online, que duró desde el 21/6/2020 hasta el
        28/1/2021. Éste curso fue mi primer contacto con el mundo IT. Finalizado
        el curso, seguí capacitándome por mi cuenta realizando más cursos, siempre orientados al desarrollo web frontend.
      </p>
      <p>
        Conseguí mi primer oportunidad laboral de la mano de{" "}
        <span>Etermax</span>. Aquí, ocupé un cargo de{" "}
        <span>Desarrollador web junior</span> en el área de{" "}
        <span>herramientas internas</span>. Participé en la creación y
        mantenimiento de sitios web como por ejemplo{" "}
        <span>Preguntados.com</span>, también ataqué fixes de aplicaciones
        corporativas y participé en el desarrollo y mantenimiento de las mismas.
        Logré adaptarme rápidamente a un equipo ágil que trabajaba con la
        metodología <span>scrum</span>, por lo cual también tengo experiencia en
        reuniones de equipo que trabajan bajo éste método. También logré una <span>buena comunicación</span> con nuestros clientes internos.
        <span> Desempeño en Etermax: (Mayo 2021 - Noviembre 2022)</span>
      </p>
      <p>
        Dentro de mis <span>habilidades de desarrollo web</span>, se encuentran
        tecnologías como:
        <ul>
          <li><span>HTML</span></li>
          <li><span>CSS</span></li>
          <li><span>Javascript</span></li>
          <li><span>React.js</span></li>
          <li><span>GIT</span></li>
        </ul>
      </p>
    </div>
  );
}

export default Description;
