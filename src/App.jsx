import React from "react";
import "./App.css";
import AppBar from "./components/AppBar/AppBar";
import Description from "./components/Description/Description";
import Technologies from "./components/Technologies/Technologies";
import Contact from "./components/Contact/Contact";
import Project from "./components/OneProject/Project";
import { techs } from "../technologies";

function App() {
  return (
    <>
      <AppBar />
      <Description />
      <Contact />
      <Technologies techs={techs} />
      <Project />
    </>
  );
}

export default App;
